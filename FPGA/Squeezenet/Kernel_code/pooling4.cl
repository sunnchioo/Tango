
__kernel void __attribute__ ((reqd_work_group_size(55,1,1)))pooling4(__global float *fire4_Features,__global float *Layer4_pool_HW)
	{
	int x = get_local_id(0);
	int y = get_group_id(0);


    float max = 0.0;
    {
        for(int output =0;output < 256 ;output++)
        {
            if(x%2 != 0)
            {
                if(y%2 != 0)
                {
                    for(int i = x-1; i <= x+1; i++)
                    {
			if(i>54) break;
                        for(int j = y-1; j <= y+1; j++)
                        {
			    if(j>54) break;
                            if(max < ((fire4_Features[output*55*55+i*55+j])))
                                max = ((fire4_Features[output*55*55+i*55+j]));

                        }
                    }
                    Layer4_pool_HW[output*27*27+((x-1)/2)*27+(y-1)/2] = max;
                    max = 0.0;
                }
            }
        }
    }

}
