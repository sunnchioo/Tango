__kernel void __attribute__ ((reqd_work_group_size(1000,1,1))) global_pooling(__global float *Layer10_Features, __global float *output_GPU)
{
	int tid = get_global_id(0);

    float avg = 0;
    for(int i = 0; i < 15; i++)
    {
        for(int j = 0; j <= 15; j++)
        {
		avg+= Layer10_Features[tid*225 + i*15 + j];
        }
    }
    output_GPU[tid] = avg/225;
}
